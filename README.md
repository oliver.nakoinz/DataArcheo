# DataArchaeo: Archaeological Data Repository

**migrated to https://codeberg.org/OliverNakoinz/DataArchaeo**

This R-package provides some archaeological data: `browseVignettes(package = "DataArchaeo")`

## Installation: 

`remotes::install_gitlab("oliver.nakoinz/DataArchSites")` or   

`devtools::install_git("git@gitlab.com:oliver.nakoinz/DataArchSites.git")`

## Direct data access

- The `inst` folder contains
    - csv-files with the data and
    - yaml-files with the metadata.

## Citation

@Manual{,  
  title = {DataArchaeo: Archaeological Data Repository for Sites},  
  author = {Oliver Nakoinz},  
  year = {2024},  
  note = {R package version 0.1.1},  
}  

## Data and names

csv with YAML-DCMI and RData with help


The data are structured according to some categories that serve as prefix for each name:

- artefacts: each row represents an artefact of a certain type, usually with the site coordinates
- structures: each row represents an archaeological structure of a certain type, usually with the site coordinates
- sites: each row represents an archaeological site
- site:  complex information from a specific site, usually hierarchical data in several files
- region:  complex information from a specific region, usually hierarchical data in several files

The data are either from one publication or compilation from different sources. The names can comprise authors, publication years and keywords. The original csv-files in the `inst` folder also contain the epsg-code. 


## This repository contains:

- artefacts_Nakoinz2004_Trichteranhaenger [Hallstattzeitliche Trichteranhänger](https://filedn.eu/lcqNcPK63RVBA8px6W2nJXB/publ/nakoinz_schr_47.pdf)

- artefacts_Nakoinz2004_Trichteranhaenger
- sites_DaenWohldMedVillages
- sites_Hingst1979_SH
- sites_PrincelySeats
- sites_Thieme1976_Holstein
- sites_Willroth1992_AngelnSchwansen
- structures_Nakoinz2003_Daemme


### todo artefacts

- artefacts_SlavonicShips     Nakoinz 1998, Slawische Schiffe
- artefacts_punts praehme     Nakoinz 2005, Mittelalterliche Prähme im Ostseeraum
- artefacts_CeramicsBraubach  Braubacher stempelverzierte Keramik
- fibulae

### todo structures

- xxx

### todo sites

- sites_Thieme1976, Südholstein
- sites_Nakoinz2004_Schlei
- sites_HofmannNakoinz2001_Schleimuendung
- sites_Nakoinz2004_SchleiFortifications
- sites_DaenWohldMonuments       Nakoinz/Knitter Dänischer Wohld monuments meg+tum


- Eichfeld
- Tuitjer


### todo site

- Bescheid
- Bassenheim
- Jevenstedt


### todo region

- xxx







## Todo

- include
    - Nakoinz 2004, Schlei
    - Nakoinz 2004, Burgen Schlei
    - Hofmann/Nakoinz 2001 Schleimündung
    - Nakoinz/Knitter Dänischer Wohld
    - Thieme 1976
    - Fürstensitze
- Daten Pipelines vorbereiten (Funktionen schreiben)
    - zu Zeonodo
    - zu JMA Data Rep
- Daten aufbereiten
- archChrono integrieren
- Automatische Datenerfassung (Digitalisierung von analogen Publikationen in Datenbanken)



