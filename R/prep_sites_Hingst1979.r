# xxxxxxxxxxxxxxxxxxxxx  Hingst 1979 xxxxxxxxxxxxxxxxxxxxxxxxxxx
## Hingst 1979 SH

filepath <- R.utils::filePath("./inst/sites_Hingst1979_Schleswig-Holstein.csv")
sites_Hingst1979_SH <- read.csv(filepath,
                                sep = ";",
                                stringsAsFactors = FALSE,
                                colClasses = c("integer",   #id
                                               "integer",   #karte
                                               "character", #type
                                               "character", #chron
                                               "integer",   #location_q
                                               "integer",   #chron_q
                                               "integer",   #info_q
                                               "character"  #WKT
                                ))
rm(filepath)
saveRDS(sites_Hingst1979_SH,
        file = "./data/sites_Hingst1979_SH.rds")

#' Archaeological sites in Schleswig-Holstein (DE) based on Hingst 1979
#'
#' A dataset based on sites from four maps, covering Bronze Age til Roman Iron Age in Schleswig-Holstein.
#' The maps have been published by Hingst 1979, Abb. 22--25. 
#' The same site can be part of several maps. Filtering for chron or type only results in duplicates. DCMI:
#' - location:        DE, Schleswig-Holstein
#' - DCMI period:     Bronze Age, Iron Age
#' - date:            2024
#' - subject:         archaeological sites
#' - language:        en, de
#' - rights:          \url{https://creativecommons.org/licenses/by/4.0/}
#' - source:          \url{https://gitlab.com/oliver.nakoinz/DataArchSites/-/blob/main/inst/sites_Hingst1979_Schleswig-Holstein.csv} (sep=";", utf-8)
#'
#' @name sites_Hingst1979_SH
#' @author Oliver Nakoinz \email{oliver.nakoinz@ufg.uni-kiel.de}
#' @docType data
#' @format A data frame with 971 rows and 8 variables:
#'   - `id`, int:     identification number
#'    - `karte`, chr:   Kartennummer (figure number)
#'    	- Abb. 22 = jüngere Bronzezeit
#'      - Abb. 23 = ältere vorrömische Eisenzeit
#'    	- Abb. 24 = jüngere vorrömische Eisenzeit
#'    	- Abb. 25 = Kaiserzeit
#'   - `type`, chr:   duration of the sites. 
#'   - `chron`, chr:  chronology string according to https://gitlab.com/oliver.nakoinz/chronsys/-/blob/main/Thieme1976.csv 
#'   - `location_q`, num: quality of localisation
#'     - 3: some m
#'     - 2: about 100 m
#'     - 1: about 1000-2000 m
#'     - 0: unknown
#'   - `chron_q`, num: quality of chronological dating
#'     - 3: save dating with precision of few phases
#'     - 2: probable dating with precision of at least of epochs (e. g. Preroman Iron Age)
#'     - 1: plausible dating to broad epochs Metal Ages
#'     - 0: no chronological information
#'   - `info_q`, int: general quality of the information comprising chron_q, location_q and general quality of observations
#'     - 3: site exists and location_q and chron_q are 2 or better
#'     - 2: site exists and location_q and chron_q are 1 or better
#'     - 1: site existence questionable or location_q or chron_q are 0 or
#'     - 0: site does not exist
#'   - `WKT`, chr:    coordinates WKT format, SRID=25832, precision=site (see column location_q for specific information)
#'
#' @source Hingst, Hans, Die vorrömische Eisenzeit. Geschichte Schleswig-Holsteins 2 (Neumünster, 1964/1979).
#' @importFrom Rdpack reprompt
NULL
