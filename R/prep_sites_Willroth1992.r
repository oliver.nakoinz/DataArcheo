## Willroth 1992 Angeln Schwansen

filepath <- R.utils::filePath("./inst/sites_Willroth1992_AngelnSchwansen_epsg32632.csv")
sites_Willroth1992_AngelnSchwansen <- read.csv(filepath,
                                               sep = ";",
                                               stringsAsFactors = FALSE,
                                               colClasses = c("integer",    #id
                                                              "character", #cat_no
                                                              "character", #name
                                                              "character", #type
                                                              "character", #chron
                                                              "character", #comment
                                                              "character", #source
                                                              "integer",   #location_q
                                                              "integer",   #chron_q
                                                              "integer",   #info_q
                                                              "character"  #WKT
                                               ))
rm(filepath)
saveRDS(sites_Willroth1992_AngelnSchwansen,
     file = "./data/sites_Willroth1992_AngelnSchwansen.rds")

#' Archaeological sites in Angeln and Schwansen (DE) based on Willroth 1992
#'
#' A dataset containing 565 sites from Bronze Age til Roman Iron Age from Angeln and Schwansen (Schleswig-Holstein, DE).
#' The sites have been published by Willroth 1992. The original publication also covers the periods including Medival times. DCMI:
#' - location:        DE, Angeln and Schwansen
#' - DCMI period:     Bronze Age, Iron Age
#' - date:            2022
#' - subject:         archaeological sites
#' - language:        en, de
#' - rights:          \url{https://creativecommons.org/licenses/by/4.0/}
#' - source:          \url{https://gitlab.com/oliver.nakoinz/DataArchSites/-/blob/main/inst/willroth_1992.csv} (sep=";")
#'
#' @name sites_Willroth1992_AngelnSchwansen
#' @author Laurenz Hillmann \email{hillmann_laurenz@gmx.de}
#' @docType data
#' @format A data frame with 565 rows and 11 variables:
#'   - `id`, int:     identification number
#'   - `cat_no`, chr: catalogue number according to Willroth
#'   - `name`, chr:   site name
#'   - `type`, chr:   site type: cemetery, flat grave, hoard, secondary burial, secondary urn burial, settlement, tumulus
#'   - `chron`, chr:  chronology string according to \url{https://gitlab.com/oliver.nakoinz/chronsys/-/blob/main/landscWillroth1992.csv} with tailing probabilities
#'   - `comment`, int: any comments, German
#'   - `source`, int: particular source information
#'   - `location_q`, num: quality of localisation
#'     - 3: some m
#'     - 2: about 100 m
#'     - 1: about 1000 m
#'     - 0: unknown
#'   - `chron_q`, num: quality of chronological dating
#'     - 3: save dating with precision of few phases
#'     - 2: probable dating with precision of at least of epochs (e. g. Preroman Iron Age)
#'     - 1: plausible dating to broad epochs Metal Ages
#'     - 0: no chronological information
#'   - `info_q`, int: general quality of the information comprising chron_q, location_q and general quality of observations
#'     - 3: site exists and location_q and chron_q are 2 or better
#'     - 2: site exists and location_q and chron_q are 1 or better
#'     - 1: site existence questionable or location_q or chron_q are 0 or
#'     - 0: site does not exist
#'   - `WKT`, chr:    coordinates WKT format, SRID=32632, precision=site (see column location_q for specific information)
#'
#' @source Willroth, K.-H., Untersuchungen zur Besiedlungsgeschichte der Landschaften Angeln und Schwansen von der älteren Bronzezeit bis zum frühen Mittelalter. Wachholtz, Neumünster, 1992.
#' @importFrom Rdpack reprompt
NULL
