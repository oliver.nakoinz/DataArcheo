
filepath <- R.utils::filePath("./inst/sites_Thieme1976_Holstein_epsg32632.csv")

sites_Thieme1976_Holstein_epsg32632 <- read.csv(filepath,
                                                sep = ";",
                                                stringsAsFactors = FALSE)

rm(filepath)
saveRDS(sites_Thieme1976_Holstein_epsg32632,
     file = "./data/sites_Thieme1976_Holstein.rds")

#' Iron Age Sites in Südholstein
#'
#' A dataset containing settlements, graves and cemeteries from the Iron Age in Südholstein according to the Magister thesis from Wulf Thieme. DCMI:
#' - location:        Südholstein, DE
#' - DCMI period:     Iron Age
#' - date:            2023
#' - subject:         sites
#' - language:        en, de
#' - rights:          \url{https://creativecommons.org/licenses/by/4.0/}
#' - source:          \url{https://gitlab.com/oliver.nakoinz/DataArchSites/-/blob/main/inst/villagesDaenWohld.csv} (sep=";")
#'
#' @name sites_Thieme1976_Holstein_epsg32632
#' @author  Philip Lüth, \email{info@lueth-archaeologie.de}; Oliver Nakoinz, \email{oliver.nakoinz@ufg.uni-kiel.de}
#' @docType data
#' @format A data frame with:
#'    - `id`, int:     identification number
#'    - `name`, chr:   site name, LA number and Kreis
#'    - `type`, chr:  Siedlung or Grab (including cemeteries)
#'    - `chron`, chr:  chronology string according to https://gitlab.com/oliver.nakoinz/chronsys/-/blob/main/Thieme1976.csv 
#'    - `source`, chr: particular source information from Thiemes thesis
#'    - `location_q`, int: quality of localisation
#'        - 3: some m
#'        - 2: about 100 m
#'        - 1: about 1000 m
#'        - 0: unknown
#'    - `chron_q`, int: quality of chronological dating
#'        - 3: save dating with precision of few phases
#'        - 2: probable dating with precision of at least of epochs
#'        - 1: plausible dating to broad epochs Metal Ages
#'        - 0: no chronological information
#'    - `info_q`, int: general quality of the information comprising chron_q, location_q and general quality of observations
#'        - 3: site exists and location_q and chron_q are 2 or better
#'        - 2: site exists and location_q and chron_q are 1 or better
#'        - 1: site existence questionable or location_q or chron_q are 0 or
#'        - 0: site does not exist
#'    - `WKT`, chr:    coordinates WKT format, SRID=32632, precision=site (see column location_q for specific information)
#'
#' @source Thieme, Wulf, Untersuchungen an eisenzeitlichen Siedlungen in Südholstein (Magisterarbeit Kiel, 1976)
#' @importFrom Rdpack reprompt
#' @example holstein <- DataArchaeo::sites_Thieme1976_Holstein_epsg32632
NULL


