## Fürstensitze

filepath <- R.utils::filePath("./inst/sites_PrincelySeats_epsg4326.csv")
sites_PrincelySeats <- read.csv(filepath,
                                sep = ";",
                                stringsAsFactors = FALSE)
rm(filepath)
saveRDS(sites_PrincelySeats,
     file = "./data/sites_PrincelySeats.rds")

#' Princly Seats of the Early Iron Age in Europe
#'
#' A dataset containing 28 sites from Ha D and Early Latène
#' The sites have been published in different lists (mentioned in the source-column). DCMI:
#' - location:        Europe
#' - DCMI period:     Early Iron Age
#' - date:            2022
#' - subject:         archaeological sites
#' - language:        en, de
#' - rights:          \url{https://creativecommons.org/licenses/by/4.0/}
#' - source:          \url{https://gitlab.com/oliver.nakoinz/DataArchSites/-/blob/main/inst/princelySeats.csv} (sep=";")
#'
#' @name sites_PrincelySeats
#' @author  Oliver Nakoinz, \email{oliver.nakoinz@ufg.uni-kiel.de}
#' @docType data
#' @format A data frame with 28 rows and 11 variables:
#'    - `id`, int:     identification number
#'    - `name`, chr:   site name
#'    - `chron`, chr:  chronology string according to https://gitlab.com/oliver.nakoinz/chronsys/-/blob/main/fortME.csv with tailing probabilities
#'    - `pgrave_hac`, int: number of Ha C princely graves in the vicinity
#'    - `pgrave_had`, int: number of Ha D princely graves in the vicinity
#'    - `pgrave_flt`, int: number of FLT princely graves in the vicinity
#'    - `source`, chr: particular source information
#'    - `location_q`, int: quality of localisation
#'        - 3: some m
#'        - 2: about 100 m
#'        - 1: about 1000 m
#'        - 0: unknown
#'    - `chron_q`, int: quality of chronological dating
#'        - 3: save dating with precision of few phases
#'        - 2: probable dating with precision of at least of epochs (e. g. Preroman Iron Age)
#'        - 1: plausible dating to broad epochs Metal Ages
#'        - 0: no chronological information
#'    - `info_q`, int: general quality of the information comprising chron_q, location_q and general quality of observations
#'        - 3: site exists and location_q and chron_q are 2 or better
#'        - 2: site exists and location_q and chron_q are 1 or better
#'        - 1: site existence questionable or location_q or chron_q are 0 or
#'        - 0: site does not exist
#'    - `WKT`, chr:    coordinates WKT format, SRID=4326, precision=site (see column location_q for specific information)
#'
#' @source different sources, see source-column
#' @importFrom Rdpack reprompt
NULL

