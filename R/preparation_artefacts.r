## Setup Package

#usethis::use_ccby_license()
#usethis::use_package("knitr")
#usethis::use_readme_md()
#usethis::use_roxygen_md(overwrite = FALSE)
#usethis::use_vignette("Artefactdata", title = "Data on Archaeological Artefacts")


## Trichteranhänger

filepath <- R.utils::filePath("./inst/artefacts_Nakoinz2004_Trichteranhaenger_epsg25832.csv")
artefacts_Nakoinz2004_Trichteranhaenger <- read.csv(filepath,
                     sep = ";",
                     stringsAsFactors = FALSE)
rm(filepath)
save(artefacts_Nakoinz2004_Trichteranhaenger,
     file = "./data/artefacts_Nakoinz2004_Trichteranhaenger.RData")


#' Trichteranhänger: Funnel pendants from the Early Iron Age in BE, DE and NL
#'
#' A dataset containing 18 funnel pendants. DCMI:
#' - location:        BE, DE, NL
#' - DCMI period:     Iron Age, Ha C zu Ha D
#' - date:            2022
#' - subject:         archaeological types
#' - language:        de, en
#' - rights:          \url{https://creativecommons.org/licenses/by/4.0/}
#' - source:          \url{https://gitlab.com/oliver.nakoinz/DataArchArtefacts/-/blob/main/inst/Trichteranhaenger.csv} (sep=";")
#'
#' @name artefacts_Nakoinz2004_Trichteranhaenger
#' @author Oliver Nakoinz \email{oliver.nakoinz.i@gmail.com}
#' @docType data
#' @format A data frame with 18 rows and 13 variables:
#'   - `id`, int:   identification number, (= number according to Nakoinz 2003/2004)
#'   - `site`, chr: Gemeinde, Fundstelle, Kreis, Land, Staat
#'   - `arch_structure`, chr: description of archaeological structures
#'   - `artefacts`, chr:  artefacts in structure
#'   - `chron`, chr:      chronology string according to \url{https://gitlab.com/oliver.nakoinz/chronsys/-/blob/main/fortME.csv} with tailing probabilities
#'   - `source`, chr:     references
#'   - `quantity`, int:   number of items (2 if "several")
#'   - `length_min`, num: minimal length of objects
#'   - `length_max`, num: maximal length of objects
#'   - `location_q`, int: quality of localisation
#'     - 3: some m
#'     - 2: about 100 m
#'     - 1: about 1000 m
#'     - 0: unknown
#'   - `chron_q`, int: quality of chronological dating
#' 	   - 3: save dating with precision of few phases
#' 	   - 2: probable dating with precision of at least of epochs (e. g. Preroman Iron Age)
#' 	   - 1: plausible dating to broad epochs Metal Ages
#' 	   - 0: no chronological information
#'   - `info_q`, int: general quality of the information comprising chron_q, location_q and general quality of observations
#' 	   - 3: item exists and location_q and chron_q are 2 or better
#' 	   - 2: item exists and location_q and chron_q are 1 or better
#' 	   - 1: item existence questionable or location_q or chron_q are 0 or
#' 	   - 0: item does not exist
#'   - `WKT`, chr:        coordinates WKT format, SRID=25832, precision=site (see column location_q for specific information)
#'
#' @source O. Nakoinz, Hallstattzeitliche Trichteranhänger am Niederrhein - eine semiotische Analyse Starigard Jahresber. FUFG 2003/2004, 2004, 145-161. (\url{https://filedn.eu/lcqNcPK63RVBA8px6W2nJXB/publ/nakoinz_schr_47.pdf})
#' @importFrom Rdpack reprompt
NULL

