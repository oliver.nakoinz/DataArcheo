## Dorfgründungen  Dänischer Wohld

filepath <- R.utils::filePath("./inst/sites_DaenWohldMedVillages_epsg4326.csv")
sites_DaenWohldMedVillages <- read.csv(filepath,
                                       sep = ";",
                                       stringsAsFactors = FALSE)
rm(filepath)
saveRDS(sites_DaenWohldMedVillages,
     file = "./data/sites_DaenWohldMedVillages.rds")


#' Medieval villages in Dänischer Wohld
#'
#' A dataset containing 13 villages from Late Medieval times with date of first mention in Dänischer Wohld. DCMI:
#' - location:        Dänischer Wohld, DE
#' - DCMI period:     Late Medieval Times
#' - date:            2022
#' - subject:         villages
#' - language:        en
#' - rights:          \url{https://creativecommons.org/licenses/by/4.0/}
#' - source:          \url{https://gitlab.com/oliver.nakoinz/DataArchSites/-/blob/main/inst/villagesDaenWohld.csv} (sep=";")
#'
#' @name sites_DaenWohldMedVillages
#' @author  Oliver Nakoinz, \email{oliver.nakoinz@ufg.uni-kiel.de}
#' @docType data
#' @format A data frame with 13 rows and 9 variables:
#'    - `id`, int:     identification number
#'    - `name`, chr:   site name
#'    - `firstMention_Laur`, chr:  first mention of the village according to W. Laur
#'    - `firstMention_wikipedia`, int:  first mention of the village according to Wikipedia
#'    - `source`, chr: particular source information
#'    - `location_q`, int: quality of localisation
#'        - 3: some m
#'        - 2: about 100 m
#'        - 1: about 1000 m
#'        - 0: unknown
#'    - `chron_q`, int: quality of chronological dating
#'        - 3: save dating with precision of few phases
#'        - 2: probable dating with precision of at least of epochs
#'        - 1: plausible dating to broad epochs Metal Ages
#'        - 0: no chronological information
#'    - `info_q`, int: general quality of the information comprising chron_q, location_q and general quality of observations
#'        - 3: site exists and location_q and chron_q are 2 or better
#'        - 2: site exists and location_q and chron_q are 1 or better
#'        - 1: site existence questionable or location_q or chron_q are 0 or
#'        - 0: site does not exist
#'    - `WKT`, chr:    coordinates WKT format, SRID=4326, precision=site (see column location_q for specific information)
#'
#' @source see source-column and O. Nakoinz and D. Knitter, Modelling Human Behaviour in Landscapes. Springer, New York 2016.
#' @importFrom Rdpack reprompt
#' @example ps <- DataArchaeo::sites_DaenWohldMedVillages
NULL
