#' Transforming and filtering of a dataframe from DataArchaeo into a geo-object or a simple object with coordinates only
#'
#' The task of this function is to easily extract DataArchaeo data for further spatial analysis. The function allows for providing multiple phases and types and returns named objects of each combination in a list.
#'
#' @param sites DataArchaeo dataframe
#' @param chron chr, chronological filter entries, single value or vector
#' @param type  chr, typological filter entries, single value or vector
#' @param location_q int, minimal level for location quality filter
#' @param chron_q    int, minimal level for chronogy quality filter
#' @param info_q     int, minimal level for information quality filter
#' @param otype chr, output type, sf: sf-object; df: dataframe with two coordinate columns only; mat: matrix with two coordinate columns only
#' @param epsg epsg code
#'
#' @return list of dataframes of marices
#' @export
#' @author Oliver Nakoinz <oliver.nakoinz@ufg.uni-kiel.de>
#'
#' @examples
#' slist <- DataArchaeo::dataArchaeo2geo(
#' sites = DataArchaeo::sites_Willroth1992_AngelnSchwansen,
#' chron = c("youngerBA", "PRIA", ">RIA"),
#' type  = NULL,
#' location_q = 1,
#' chron_q    = 1,
#' info_q     = 2,
#' otype      = "df",
#' epsg       = 25832)
#' slist[[3]]
#' names(slist)
dataArchaeo2geo <- function(
        sites,
        chron = NULL,
        type  = NULL,
        location_q = 1,
        chron_q = 1,
        info_q  = 1,
        otype   = c("sf", "df", "mat"),
        epsg    = 25832) {
    chron <- c("", chron)
    type <- c("", type)
    dfnames <- rep("df", length(chron)*length(type))
    res <- list()
    k = 0
    for (i in 1:length(chron)) {
        for (j in 1:length(type)) {
            k <- k + 1
            if (chron[i] == ""){cname <- "all"} else {cname <- chron[i]}
            if (type[j]  == ""){tname <- "all"} else {tname <- type[j]}
            dfnames[k] <- paste0("c_", cname, "_t_", tname)
            selind <- grepl(chron[i], sites$chron) & grepl(type[j], sites$type) & sites$location_q >= location_q & sites$chron_q >= chron_q & sites$info_q >= info_q
            selsites <- sites[selind,]
            selsites <- sf::st_as_sf(x = selsites,
                                     wkt = "WKT",
                                     crs = sf::st_crs(epsg))
            if (otype == "sf")  {
                res[[k]] <- selsites
            }
            if (otype == "df")  {
                selsites <- data.frame(
                    x = sf::st_coordinates(selsites)[,1],
                    y = sf::st_coordinates(selsites)[,2])
                res[[k]] <- selsites
            }
            if (otype == "mat") {
                selsites <- data.frame(
                    x = sf::st_coordinates(selsites)[,1],
                    y = sf::st_coordinates(selsites)[,2])
                selsites <- as.matrix(selsites)
                res[[k]] <- selsites
            }
        }
    }
    names(res) <- dfnames
    return(res)
}
